<?php

// Special configuration file for servers using Redis.
// Overwrites $TYPO3_CONF_VARS on the fly and is therefore loaded after
// any other configuration files.

if (class_exists('redis')) {
    $cacheTables = [
        // Database numbers 0 and 1 are used and flushed by the core unit tests
        // and should not be used if possible
        '',
        '',
        'hash',
        'imagesizes',
        'pages',
        'pagesection',
        'rootline',
    ];

    $context = (string)\TYPO3\CMS\Core\Core\Environment::getContext();

    switch ($context) {
        case 'Development/ddev':
            $defaultOptions = [
                'hostname' => 'redis',
            ];
            break;
        case 'Development/k8s':
        case 'Staging/k8s':
        case 'Testing/k8s':
        case 'Production/k8s':
            $defaultOptions = [
                'hostname' => getenv('REDIS_HOST'),
                'port'     => getenv('REDIS_PORT') ?: 6379,
            ];
            break;
        default:
            $defaultOptions = [
                'hostname' => '127.0.0.1',
            ];
            break;
    }

    // Enable persistent connections
    $defaultOptions['persistentConnection'] = 1;

    // Disable compression
    $defaultOptions['compression'] = false;

    // Override default settings if environment variables for socket and password are set:

    $redisSocket = getenv('REDIS_SOCKET');
    if ($redisSocket) {
        $defaultOptions['hostname'] = $redisSocket;
        $defaultOptions['port'] = 0; // php-redis treats hostname like a socket if the port is 0
    }

    $redisPassword = getenv('REDIS_PASSWORD');
    if ($redisPassword) {
        $defaultOptions['password'] = $redisPassword;
    }

    foreach ($cacheTables as $index => $cacheTable) {
        if ($cacheTable === '') {
            continue;
        }
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$cacheTable]['backend'] = 'TYPO3\CMS\Core\Cache\Backend\RedisBackend';
        $localOptions = [
            'database' => $index,
        ];
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$cacheTable]['options'] = array_merge(
            (array)$GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$cacheTable]['options'],
            $defaultOptions,
            $localOptions
        );
    }

    // Configure Redis locking via b13/distributed-locks
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locking']['redis'] = [
        'hostname' => $defaultOptions['hostname'],
        'database' => count($cacheTables),
        'port' => $defaultOptions['port'] ?? 6379,
        'authentication' => $redisPassword,
        'ttl' => ini_get('max_execution_time') ?: 60,
    ];

    unset($cacheTables);

    /*
    // Make a test connection and turn off RedisLockingStrategy if the connection fails.
    try {
        $redisTest = new \Redis();
        $connectionTimeout = 0;
        if (isset($defaultOptions['persistentConnection']) && $defaultOptions['persistentConnection']) {
            $isConnected = $redisTest->pconnect($defaultOptions['hostname'], $defaultOptions['port'], $connectionTimeout, 'redis_test');
        } else {
            $isConnected = $redisTest->connect($defaultOptions['hostname'], $defaultOptions['port'], $connectionTimeout);
        }
    } catch (\Exception $e) {
        // Turn off the RedisLockingStrategy if the server is not available.
        // Additionally, log this error.
        \TYPO3\CMS\Core\Utility\GeneralUtility::sysLog('Could not connect to redis server.', 'core', \TYPO3\CMS\Core\Utility\GeneralUtility::SYSLOG_SEVERITY_ERROR);
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['locking']['redis']['priority'] = 0;
    }
    */
}
